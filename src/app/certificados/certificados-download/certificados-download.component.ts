import { CertificadosService } from './../certificados.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Certificados } from './../certificados.model';

@Component({
  selector: 'app-certificados-download',
  templateUrl: './certificados-download.component.html'
})

export class CertificadosDownloadComponent implements OnInit, OnDestroy {

  cert: any;
  certificados: Certificados[];
  inscricao: Subscription;
  cpf: number;

  constructor(
    public certificadosService: CertificadosService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  /* METODO QUE RETORNA OS CERTIFICADOS PELO CPF DIGITADO NO INPUT*/
  ngOnInit() {
    this.inscricao = this.route.params.subscribe(
      (params: any) => {
        this.cpf = params['cpf'];
        const cert = this.certificadosService.getCertificados(this.cpf)
          .subscribe((certificados) => { this.certificados = certificados; });
        if (this.cert !== null) {
          return cert;
        } else {
          console.log('Certificado não encontrado');
          return null;
        }
      });
  }

  /* DESTROI A INSCRICAO FEITA NA ROTA */
  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/* RXJS */
import { Observable } from 'rxjs/Observable';

/* url */
import { URL_API } from './../app.api-url';

/* MODEL */
import { Certificados } from './certificados.model';


@Injectable()
export class CertificadosService {

  constructor(
    private http: HttpClient
  ) { }

  getCertificados(cpf: number): Observable<Certificados[]> {
    return this.http.get<Certificados[]>(`${URL_API}/certificados-download/${cpf}`, { responseType: 'json' });
  }
}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from './../angular-material/angular-material.module';

import { CertificadosService } from './certificados.service';

import { CertificadosRoutingModule } from './certificados-routing.module';

import { CertificadosDownloadComponent } from './certificados-download/certificados-download.component';
import { CertificadosComponent } from './certificados.component';
import { CertificadoBuscaComponent } from './certificado-busca/certificado-busca.component';


@NgModule({
  imports: [
    CommonModule,
    AngularMaterialModule,
    CertificadosRoutingModule
  ],
  declarations: [
    CertificadosComponent,
    CertificadosDownloadComponent,
    CertificadoBuscaComponent
  ],
  providers: [CertificadosService]
})
export class CertificadosModule { }

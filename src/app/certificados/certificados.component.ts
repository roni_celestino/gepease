import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
import { NavigationEnd, ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

@Component({
    selector: 'app-certificados',
    templateUrl: './certificados.component.html'
})

export class CertificadosComponent implements OnInit {

    constructor(
        private http: HttpClient,
        private titleService: Title,
        private router: Router,
        private activatedRoute: ActivatedRoute,
    ) {

    }

    ngOnInit() {
        this.router.events
            .filter((event) => event instanceof NavigationEnd)
            .map(() => this.activatedRoute)
            .map((route) => {
                while (route.firstChild) {
                    route = route.firstChild;
                    return route;
                }
            })
            .filter((route) => route.outlet === 'primary')
            .mergeMap((route) => route.data)
            .subscribe((event) => this.titleService.setTitle(event['title']));
    }

}


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CertificadosComponent } from './certificados.component';
import { CertificadosDownloadComponent } from './certificados-download/certificados-download.component';
import { CertificadoBuscaComponent } from './certificado-busca/certificado-busca.component';

const routes: Routes = [
  {
    path: '', component: CertificadosComponent, children: [
      { path: '', redirectTo: 'busca', data: { title: 'GEPEASE | Busca de Certificados' } },
      { path: 'busca', component: CertificadoBuscaComponent, data: { title: 'GEPEASE | Busca de Certificados' } },
      { path: ':cpf', component: CertificadosDownloadComponent, data: { title: 'GEPEASE | Baixar Certificado' } }
    ], data: { title: 'GEPEASE | Busca de Certificados' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CertificadosRoutingModule { }

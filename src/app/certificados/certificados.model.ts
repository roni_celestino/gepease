export interface Certificados {
    id: number;
    nome: string;
    tipo: string;
    certificado: string;
    cpf: number;
    created_at: string;
    updated_at: string;
}


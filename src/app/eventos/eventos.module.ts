import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from './../angular-material/angular-material.module';

import { EventosRoutingModule } from './eventos-routing.module';

import { EventosService } from './eventos.service';

import { EventosComponent } from './eventos.component';
import { EventoComponent } from './evento/evento.component';
import { EventoDetalheComponent } from './evento-detalhe/evento-detalhe.component';
import { EventosApresentacaoComponent } from './evento-detalhe/eventos-apresentacao/eventos-apresentacao.component';
import { EventosProgramacaoComponent } from './evento-detalhe/eventos-programacao/eventos-programacao.component';
import { EventosOficinasComponent } from './evento-detalhe/eventos-oficinas/eventos-oficinas.component';
import { EventosTrabalhosComponent } from './evento-detalhe/eventos-trabalhos/eventos-trabalhos.component';
import { EventosComissaoComponent } from './evento-detalhe/eventos-comissao/eventos-comissao.component';

@NgModule({
  imports: [
    CommonModule,
    AngularMaterialModule,
    EventosRoutingModule
  ],
  declarations: [
    EventosComponent,
    EventoDetalheComponent,
    EventoComponent,
    EventoComponent,
    EventosApresentacaoComponent,
    EventosProgramacaoComponent,
    EventosOficinasComponent,
    EventosTrabalhosComponent,
    EventosComissaoComponent,
  ],
  providers: [EventosService]
})
export class EventosModule { }

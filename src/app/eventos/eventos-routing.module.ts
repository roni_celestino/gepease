import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EventosComponent } from './eventos.component';
import { EventoDetalheComponent } from './evento-detalhe/evento-detalhe.component';
import { EventosApresentacaoComponent } from './evento-detalhe/eventos-apresentacao/eventos-apresentacao.component';
import { EventosProgramacaoComponent } from './evento-detalhe/eventos-programacao/eventos-programacao.component';
import { EventosOficinasComponent } from './evento-detalhe/eventos-oficinas/eventos-oficinas.component';
import { EventosTrabalhosComponent } from './evento-detalhe/eventos-trabalhos/eventos-trabalhos.component';
import { EventosComissaoComponent } from './evento-detalhe/eventos-comissao/eventos-comissao.component';

const routes: Routes = [
  { path: '', component: EventosComponent, data: { title: 'GEPEASE | Eventos' } },
  {
    path: 'eventos-anteriores/:id', component: EventoDetalheComponent,
    children: [
      { path: '', redirectTo: 'apresentacao', pathMatch: 'full' },
      { path: 'apresentacao', component: EventosApresentacaoComponent, data: { title: 'GEPEASE | Apresentação' } },
      { path: 'programacao-old', component: EventosProgramacaoComponent, data: { title: 'GEPEASE | Programação' } },
      { path: 'oficinas', component: EventosOficinasComponent, data: { title: 'GEPEASE | Oficinas' } },
      { path: 'trabalhos', component: EventosTrabalhosComponent, data: { title: 'GEPEASE | Trabalhos' } },
      { path: 'comissao', component: EventosComissaoComponent, data: { title: 'GEPEASE | Comissão' } },
    ],
    data: { title: 'GEPEASE | Eventos' }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventosRoutingModule { }

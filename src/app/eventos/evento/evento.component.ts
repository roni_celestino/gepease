import { Component, OnInit, Input } from '@angular/core';

import { Eventos } from './eventos.model';

@Component({
  selector: 'app-evento',
  templateUrl: './evento.component.html'
})
export class EventoComponent implements OnInit {

  @Input() evento: Eventos;

  constructor() { }

  ngOnInit() {
  }

}

export interface Eventos {
    id: number;
    titulo: string;
    data: string;
    apresentacao: string;
    imgUrl: string;
}


import { Component, OnInit } from '@angular/core';

import { Eventos } from './evento/eventos.model';
import { EventosService } from './eventos.service';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html'
})
export class EventosComponent implements OnInit {

  eventos: Eventos[];

  constructor(
    private eventosService: EventosService
  ) { }

  ngOnInit() {
    return this.eventosService.eventos()
      .subscribe(
        (eventos) => { this.eventos = eventos; }
      );
  }

}

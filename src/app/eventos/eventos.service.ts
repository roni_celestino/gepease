
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { URL_API } from '../app.api-url';

import { Eventos } from './evento/eventos.model';

import {
  ProgramacaoDias,
  Programacao,
  ProgramacaoMinistrantes,
  ProgramacaoCoordenadores
} from './evento-detalhe/eventos-programacao/eventos-programacao.model';

import {
  Comissao,
  ComissaoIntegrantes
} from './evento-detalhe/eventos-comissao/comissao.model';


@Injectable()
export class EventosService {

  constructor(
    private http: HttpClient,
  ) { }


  eventos(): Observable<any> {
    return this.http.get<any>(`${URL_API}/eventos`, { responseType: 'json' });
  }

  eventosById(id: number): Observable<Eventos> {
    return this.http.get<Eventos>(`${URL_API}/evento/${id}`, { responseType: 'json' });
  }

  apresentacaoEvento(id: number): Observable<Eventos[]> {
    return this.http.get<Eventos[]>(`${URL_API}/evento/${id}`, { responseType: 'json' });
  }

  programacaoEvento(eventos_id: number): Observable<ProgramacaoDias[]> {
    return this.http.get<ProgramacaoDias[]>(`${URL_API}/programacao/${eventos_id}`, { responseType: 'json' });
  }

  comissaoEvento(eventos_id: number): Observable<Comissao[]> {
    return this.http.get<Comissao[]>(`${URL_API}/comissoes/${eventos_id}`, { responseType: 'json' });
  }


}

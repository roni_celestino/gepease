import { Component, OnInit, Input, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { EventosService } from './../eventos.service';
import { Eventos } from './../evento/eventos.model';


@Component({
  selector: 'app-evento-detalhe',
  templateUrl: './evento-detalhe.component.html'
})

export class EventoDetalheComponent implements OnInit {

  @Input() evento: Eventos;

  constructor(
    public eventosService: EventosService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.eventosService.eventosById(this.route.snapshot.params['id'])
      .subscribe((evento) => { this.evento = evento; });
  }

}


export interface ProgramacaoDias {
    id: number;
    eventos_id: number;
    data: string;
    created_at: string;
    updated_at: string;
    programacao: Programacao[];
}

export interface Programacao {
    id: number;
    eventos_id: number;
    titulo: string;
    tema: string;
    objetivo: string;
    detalhes: string;
    resumo: string;
    requisitos: string;
    data_inicio: string;
    data_final: string;
    hora_inicio: string;
    hora_final: string;
    duracao: string;
    vagas: string;
    local: string;
    created_at: string;
    updated_at?: any;
    programacao_coordenadores: ProgramacaoCoordenadores[];
    programacao_ministrantes: ProgramacaoMinistrantes[];
}

export interface ProgramacaoCoordenadores {
    id: number;
    programacao_id: number;
    eventos_id: number;
    nome: string;
    created_at?: any;
    updated_at?: any;
}

export interface ProgramacaoMinistrantes {
    id: number;
    programacao_id: number;
    eventos_id: number;
    nome: string;
    created_at?: any;
    updated_at?: any;
}

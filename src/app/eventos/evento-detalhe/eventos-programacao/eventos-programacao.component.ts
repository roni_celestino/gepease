import { Component, OnInit } from '@angular/core';
import { EventosService } from '../../eventos.service';
import { ActivatedRoute } from '@angular/router';
import {
  ProgramacaoDias,
  Programacao,
  ProgramacaoCoordenadores,
  ProgramacaoMinistrantes
} from './eventos-programacao.model';

@Component({
  selector: 'app-eventos-programacao',
  templateUrl: './eventos-programacao.component.html'
})

export class EventosProgramacaoComponent implements OnInit {

  programacaoDias: ProgramacaoDias[];
  programacao: Programacao[];
  programacaoCoordenadores: ProgramacaoCoordenadores[];
  programacaoMinistrantes: ProgramacaoMinistrantes[];

  constructor(
    private eventosService: EventosService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.eventosService.programacaoEvento(this.route.parent.snapshot.params['id'])
      .subscribe((programacaoDias) => {
        this.programacaoDias = programacaoDias, console.log(programacaoDias);
      });
  }
}

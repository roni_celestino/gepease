import { Component, OnInit } from '@angular/core';
import { EventosService } from '../../eventos.service';
import { ActivatedRoute } from '@angular/router';

import {
  Comissao,
  ComissaoIntegrantes
} from './comissao.model';


@Component({
  selector: 'app-eventos-comissao',
  templateUrl: './eventos-comissao.component.html'
})
export class EventosComissaoComponent implements OnInit {

  comissoes: Comissao[];
  comissaoIntegrantes: ComissaoIntegrantes[];

  constructor(
    private eventosService: EventosService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.eventosService.comissaoEvento(this.route.parent.snapshot.params['id'])
      .subscribe((comissoes) => {
        this.comissoes = comissoes, console.log(comissoes);
      });
  }

}

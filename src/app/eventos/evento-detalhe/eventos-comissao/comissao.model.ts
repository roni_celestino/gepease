export interface Comissao {
    id: number;
    eventos_id: number;
    titulo: string;
    descircao: string;
    created_at: string;
    updated_at: string;
    comissao_integrantes: ComissaoIntegrantes[];
}

export interface ComissaoIntegrantes {
    id: number;
    comissao_id: number;
    nome: string;
    created_at: string;
    updated_at: string;
}

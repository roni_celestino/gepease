import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs/Observable';

import { EventosService } from './../../eventos.service';
import { Eventos } from './../../evento/eventos.model';


@Component({
  selector: 'app-eventos-apresentacao',
  templateUrl: './eventos-apresentacao.component.html'
})

export class EventosApresentacaoComponent implements OnInit {

  apresentacao: Eventos;

  constructor(
    private eventosService: EventosService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.eventosService.eventosById(this.route.parent.snapshot.params['id'])
      .subscribe((apresentacao) => { this.apresentacao = apresentacao; });
  }
}

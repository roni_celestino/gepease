
import { NgModule, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, CanActivateChild, CanDeactivate, RouterLink, PreloadAllModules } from '@angular/router';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressRouterModule } from '@ngx-progressbar/router';

import { EventosModule } from './eventos/eventos.module';
import { CertificadosModule } from './certificados/certificados.module';

//  COMPONENTES
import { HomeComponent } from './home/home.component';
import { SobreComponent } from './sobre/sobre.component';
import { ContatoComponent } from './contato/contato.component';
import { InscricoesComponent } from './inscricoes/inscricoes.component';
import { ProgramacaoComponent } from './programacao/programacao.component';
import { SubmissaoDeTrabalhosComponent } from './submissao-de-trabalhos/submissao-de-trabalhos.component';
import { OficinasComponent } from './oficinas/oficinas.component';
import { ComissaoComponent } from './comissao/comissao.component';
import { IndexacaoComponent } from './indexacao/indexacao.component';
import { EditorialComponent } from './editorial/editorial.component';
import { TrabalhosAprovadosComponent } from './trabalhos-aprovados/trabalhos-aprovados.component';


export const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent, data: { preload: true, title: 'GEPEASE | Home' } },
  { path: 'apresentacao', component: SobreComponent, data: { preload: true, title: 'GEPEASE | Apresentação' } },
  { path: 'programacao', component: ProgramacaoComponent, data: { preload: true, title: 'GEPEASE | Programação' } },
  { path: 'oficinas', component: OficinasComponent, data: { preload: true, title: 'GEPEASE | Oficinas' } },
  { path: 'inscricoes', component: InscricoesComponent, data: { preload: true, title: 'GEPEASE | Inscrições' } },
  { path: 'submissao-de-trabalhos', component: SubmissaoDeTrabalhosComponent, data: { preload: true, title: 'GEPEASE | Normas de submissão' } },
  { path: 'trabalhos-aprovados', component: TrabalhosAprovadosComponent, data: { preload: true, title: 'GEPEASE | Trabalhos Aprovados' } },
  { path: 'comissao', component: ComissaoComponent, data: { preload: true, title: 'GEPEASE | Comissão' } },
  { path: 'indexacao', component: IndexacaoComponent, data: { preload: true, title: 'GEPEASE | Indexação' } },
  { path: 'editorial', component: EditorialComponent, data: { preload: true, title: 'GEPEASE | Equipe Editorial' } },
  { path: 'contato', component: ContatoComponent, data: { preload: true, title: 'GEPEASE | Contato' } },
  { path: 'eventos-anteriores', loadChildren: 'app/eventos/eventos.module#EventosModule', data: { preload: true, title: 'GEPEASE | Eventos' } },
  { path: 'certificados', loadChildren: 'app/certificados/certificados.module#CertificadosModule', data: { preload: true, title: 'GEPEASE | Certificados' } }
];


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes,
      { preloadingStrategy: PreloadAllModules })
  ],
  exports: [
    RouterModule
  ]
})

export class AppRouting { }

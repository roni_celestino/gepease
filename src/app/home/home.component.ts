import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {

  text: any = {
    Year: 'Anos',
    Month: 'Mêses',
    Weeks: 'Semanas',
    Days: 'Dias',
    Hours: 'Horas',
    Minutes: 'Minutos',
    Seconds: 'Segundos',
    MilliSeconds: 'Milissegundos'
  };


  ngOnInit() {
  }


}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel-slide-home',
  templateUrl: './carousel-slide-home.component.html',
  styleUrls: ['./carousel-slide-home.component.css']
})
export class CarouselSlideHomeComponent implements OnInit {

  constructor() { }

  config: SwiperOptions = {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 30,
    // loop: true,
    // autoplay: 5000
  };

  ngOnInit() {
  }

}

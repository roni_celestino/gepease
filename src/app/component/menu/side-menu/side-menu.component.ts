import { Component, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { AppComponent } from './../../../app.component';
import { SidenavService } from './../../../side-nav.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})

export class SideMenuComponent implements OnInit {

  constructor(private sidenavService: SidenavService) { }

  closeActive = false;

  closeSidenav() {
    this.closeActive = !this.closeActive;
    this.sidenavService.close();

    console.log('Clicked');
  }

  ngOnInit(): void {

  }
}

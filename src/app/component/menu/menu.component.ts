import { Component, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { AppComponent } from './../../app.component';
import { SidenavService } from './../../side-nav.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  constructor(private sidenavService: SidenavService) { }

  toggleActive = false;

  toggleSidenav() {
    this.toggleActive = !this.toggleActive;
    this.sidenavService.toggle();

    console.log('Clicked');
  }

  ngOnInit(): void {

  }

}

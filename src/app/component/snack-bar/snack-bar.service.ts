import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';

@Injectable()
export class SnackBarService {

  constructor(
    public snackBar: MatSnackBar) { }

  showSnackBar(msg: string, action: string) {
    this.snackBar.open(msg, action, {
      duration: 4000,
    });
  }

}

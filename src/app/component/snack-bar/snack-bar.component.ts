import { Component, OnInit } from '@angular/core';
import { SnackBarService } from './snack-bar.service';


@Component({
  selector: 'app-snack-bar',
  templateUrl: './snack-bar.component.html'
})
export class SnackBarComponent implements OnInit {

  constructor(
    public snackBarService: SnackBarService
  ) { }

  ngOnInit() {
  }

}

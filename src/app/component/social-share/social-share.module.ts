import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { SocialShareComponent } from './social-share.component';
import { ShareModule } from '@ngx-share/core';
import { ShareButtonsOptions } from '@ngx-share/core';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { IShareButtons } from '@ngx-share/core';

const prop: IShareButtons = {
  facebook: {
    icon: 'fab fa-facebook-f',
    text: 'Facebook'
  },
  twitter: {
    icon: 'fab fa-twitter',
    text: 'Twitter'
  },
  google: {
    icon: 'fab fa-google',
    text: 'Google Plus'
  },
  linkedin: {
    icon: 'fab fa-linkedin-in',
    text: 'Linkedin'
  },
  pinterest: {
    icon: 'fab fa-pinterest-p',
    text: 'Pinterest'
  },
  reddit: {
    icon: 'fab fa-reddit-alien',
    text: 'Reddit'
  },
  tumblr: {
    icon: 'fab fa-tumblr',
    text: 'Tumblr'
  },
  whatsapp: {
    icon: 'fab fa-whatsapp',
    text: 'Whatsapp'
  },
  telegram: {
    icon: 'fab fa-telegram-plane',
    text: 'Telegram'
  },
  vk: {
    icon: 'fab fa-vk',
    text: 'Vk'
  },
  stumble: {
    icon: 'fab fa-stumbleupon',
    text: 'Vk'
  },
};

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    ShareButtonsModule,
    ShareButtonsModule.forRoot({ prop: prop })
  ],
  exports: [
    SocialShareComponent,
    ShareButtonsModule
  ],
  declarations: [
    SocialShareComponent
  ]
})
export class SocialShareModule { }

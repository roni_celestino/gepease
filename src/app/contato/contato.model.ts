class ContatoModel {
    constructor(
        public nome: string,
        public email: string,
        public telefone: string,
        public mensagem: string
    ) { }

}

export { ContatoModel };

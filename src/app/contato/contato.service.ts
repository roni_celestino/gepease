import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/* RXJS */
import { Observable } from 'rxjs/Observable';


/* URL */
import { URL_API } from '../app.api-url';

/* MODEL */
import { ContatoModel } from './contato.model';

@Injectable()
export class ContatoService {

  constructor(
    private http: HttpClient,
  ) { }

  enviarEmail(form: ContatoModel): Observable<any> {
    return this.http.post<ContatoModel>(`${URL_API}/enviaremail`, form);
  }

}

import { Component, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { NgForm, FormGroup, FormControl, FormBuilder, NgModel, FormControlName, Validators } from '@angular/forms';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

import 'rxjs/add/operator/catch';

import { MatSnackBar } from '@angular/material';

import { ContatoService } from './contato.service';

import { ContatoModel } from './contato.model';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html'
})

export class ContatoComponent implements OnInit {

  formulario: FormGroup;
  action = '';

  constructor(
    public snackBar: MatSnackBar,
    private contatoService: ContatoService,
    private formBuilder: FormBuilder,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.formulario = this.formBuilder.group({
      nome: [null, [Validators.required, Validators.minLength(3)]],
      email: [null, [Validators.required, Validators.email]],
      telefone: [null, [Validators.required, Validators.minLength(8)]],
      mensagem: [null, Validators.required],
    });
  }

  onSubmit(form: NgForm) {
    this.contatoService.enviarEmail(this.formulario.value)
      .map(res => res)
      .subscribe((data: string) => {
        console.log(data);
        this.openSnackBar();
        this.resetar();
      });
  }

  resetar() {
    this.formulario.reset();
  }

  openSnackBar() {
    const message = 'Formulário Enviado com Sucess';
    this.snackBar.open(message, this.action, {
      duration: 10000,
    });
  }

}

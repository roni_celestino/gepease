import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatSidenav } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { NgProgress, NgProgressRef } from '@ngx-progressbar/core';
import { SidenavService } from './side-nav.service';

import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy {

  @ViewChild('sidenav') public sidenav: MatSidenav;

  progressRef: NgProgressRef;
  state: {
    speed: 500;
  };
  constructor(
    private http: HttpClient,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private sidenavService: SidenavService,
    public progress: NgProgress,
    private titleService: Title,
  ) { }

  ngOnInit() {

    this.progressRef = this.progress.ref();
    this.progressRef.start();


    this.sidenavService.setSidenav(this.sidenav);

    this.router.events
      .filter((event) => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map((route) => {
        while (route.children) {
          route = route.firstChild;
          return route;
        }
      })
      .filter((route) => route.outlet === 'primary')
      .mergeMap((route) => route.data)
      .subscribe((event) => this.titleService.setTitle(event['title']));
  }

  ngOnDestroy() {
    // Destroy NgProgressRef instance using `NgProgress` service.
    this.progress.destroy();

    // DO NOT DESTROY USING `progressRef` ITSELF.
    // this.progressRef.destroy();
  }
}



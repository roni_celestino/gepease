import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormBuilder, ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/* ROTAS */
import { AppRouting } from './app-routing.module';

/* MODULOS */
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { SwiperModule } from 'angular2-useful-swiper';
import { SocialShareModule } from './component/social-share/social-share.module';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpClientModule } from '@ngx-progressbar/http-client';
import { NgProgressRouterModule } from '@ngx-progressbar/router';
import 'hammerjs';


/* SERVICOS */
import { ContatoService } from './contato/contato.service';
import { SidenavService } from './side-nav.service';
import { SnackBarService } from './component/snack-bar/snack-bar.service';


/* COMPONENTES */
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { MenuComponent } from './component/menu/menu.component';
import { FooterComponent } from './component/footer/footer.component';
import { SobreComponent } from './sobre/sobre.component';
import { ContatoComponent } from './contato/contato.component';
import { ProgramacaoComponent } from './programacao/programacao.component';
import { InscricoesComponent } from './inscricoes/inscricoes.component';
import { SubmissaoDeTrabalhosComponent } from './submissao-de-trabalhos/submissao-de-trabalhos.component';
import { SnackBarComponent } from './component/snack-bar/snack-bar.component';
import { OficinasComponent } from './oficinas/oficinas.component';
import { ComissaoComponent } from './comissao/comissao.component';
import { IndexacaoComponent } from './indexacao/indexacao.component';
import { EditorialComponent } from './editorial/editorial.component';
import { TrabalhosAprovadosComponent } from './trabalhos-aprovados/trabalhos-aprovados.component';


import { RealizacaoComponent } from './component/realizacao/realizacao.component';
import { ParceirosComponent } from './component/parceiros/parceiros.component';
import { CarouselSlideHomeComponent } from './component/carousel-slide-home/carousel-slide-home.component';
import { SideMenuComponent } from './component/menu/side-menu/side-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    FooterComponent,
    SobreComponent,
    ProgramacaoComponent,
    InscricoesComponent,
    ComissaoComponent,
    SubmissaoDeTrabalhosComponent,
    TrabalhosAprovadosComponent,
    ContatoComponent,
    SnackBarComponent,
    OficinasComponent,
    IndexacaoComponent,
    EditorialComponent,
    RealizacaoComponent,
    ParceirosComponent,
    CarouselSlideHomeComponent,
    SideMenuComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SwiperModule,
    SocialShareModule,
    AngularMaterialModule,
    AppRouting,
    NgProgressModule.forRoot(),
    NgProgressRouterModule
  ],
  providers: [
    Title,
    SidenavService,
    ContatoService,
    SnackBarService
  ],
  entryComponents: [
    SnackBarComponent
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
